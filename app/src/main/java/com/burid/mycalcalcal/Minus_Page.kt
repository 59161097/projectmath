package com.burid.mycalcalcal

import android.R.attr.button
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_minus__page.*
import org.w3c.dom.Text
import kotlin.random.Random


class Minus_Page : AppCompatActivity() {
    private lateinit var tex: Text
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minus__page)

//         TextView helloTextView = (TextView) findViewById(R.id.txtNumber1);
//        helloTextView.setText(R.string.user_greeting);


        txtNumber1.setText(randomInt1.toString())
        txtNumber2.setText(randomInt2.toString())

      createChoice()


    }

    val randomInt1 = Random.nextInt(1, 9)
    val randomInt2 = Random.nextInt(1, 9)
    val sum = randomInt1 + randomInt2


    fun createChoice() {
        var choice1 = 0
        var choice2 = 0
        var choice3 = 0

        val position = Random.nextInt(1, 4)
        if (position === 1) {
            btn1.setText((sum).toString())
            btn2.setText((sum + 1).toString())
            btn3.setText((sum + 2).toString())

            btn1.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ถูกต้อง", Toast.LENGTH_SHORT).show();
                addCorrect()
                txtTrue.setText((correct).toString())
            })

            btn2.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })

            btn3.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })


        } else if (position === 2) {
            btn1.setText((sum - 1).toString())
            btn2.setText((sum).toString())
            btn3.setText((sum + 2).toString())

            btn2.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ถูกต้อง", Toast.LENGTH_SHORT).show();
                addCorrect()
                txtTrue.setText((correct).toString())
            })

            btn1.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })

            btn3.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })
        } else if (position === 3) {
            btn1.setText((sum - 2).toString())
            btn2.setText((sum + 1).toString())
            btn3.setText((sum).toString())

            btn3.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ถูกต้อง", Toast.LENGTH_SHORT).show();
                addCorrect()
                //     isAddScore = false
                txtTrue.setText((correct).toString())
                ///      disableAllBtn()
                //   changeBackgroundCorrect(btn)
                ///      contDownForNewGame()
            })

            btn2.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })

            btn1.setOnClickListener(View.OnClickListener {
                Toast.makeText(applicationContext, "ผิดเลย", Toast.LENGTH_SHORT).show();
                addWrong()
                txtWrong.setText((wrong).toString())
            })
        }
    }


    var correct: Int = 0
    var wrong: Int = 0
    val amoutCorrect: Int = 0
    val amoutWrong: Int = 0


    fun addCorrect() {
        correct++
    }

    fun addWrong() {
        wrong++
    }


    private fun getScore() {
        correct = amoutCorrect
        wrong = amoutWrong
    }




}


